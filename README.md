
# **Fiber**

`Fiber` is an R package designed for the analysis of DNA modifications detected by long-read sequencing. It provides efficient tools to process, analyze, and visualize data from long-read sequencing experiments, facilitating the study of nucleosome occupancy.  

Fiber-seq is a cutting-edge genomic technique designed to analyze long molecules of DNA, known as individual chromatin fibers, at single-molecule resolution. It combines enzymatic DNA modification followed by long-read sequencing, providing insights into the epigenetic status, nucleosome positioning, and other structural features of chromatin.  

In its current version, `fiber` utilizes the results from two [Oxford Nanopore](https://nanoporetech.com/) tools, [Dorado](https://github.com/nanoporetech/dorado) and [Modkit](https://github.com/nanoporetech/modkit), to create a container for the experiment - `Fiber` object that stores the results of a single sequencing run. It also provides tools for further analysis, including merging experiments, calculating statistics, and studying DNA modifications and occupancy.  
It is also possible to adapt the package for other long-read sequencing platforms and tools. We hope to incorporate this functionality into `fiber` in future versions.

For more details, please visit the [Documentation](https://mglab.gitlab.io/fiber-docs/).

You can directly navigate to the relevant section of the documentation:

- [Installation](https://mglab.gitlab.io/fiber-docs/articles/installation.html)
- [Reference](https://mglab.gitlab.io/fiber-docs/reference/index.html) - the list of available functions for the analysis and their usage details.
- Usage:
  - [Long-read sequencing workflow](https://mglab.gitlab.io/fiber-docs/articles/seq_workflow.html)
  - [What is a Fiber object?](https://mglab.gitlab.io/fiber-docs/articles/fiber_object.html)
  - [Import sequencing results into a Fiber](https://mglab.gitlab.io/fiber-docs/articles/imports.html)
  - [How to make an HTML report?](https://mglab.gitlab.io/fiber-docs/articles/html_report.html)
- [More Analysis Examples](https://mglab.gitlab.io/fiber-docs/articles/more_analysis.html):
  - [A Fiber Toy Example](https://mglab.gitlab.io/fiber-docs/articles/fiber_toy_example.html)
  - [Viewing information stored in a Fiber](https://mglab.gitlab.io/fiber-docs/articles/viewing_info.html)
  - [Combining samples in a Fiber](https://mglab.gitlab.io/fiber-docs/articles/pooling.html)
  - [Filtering Sequencing Reads](https://mglab.gitlab.io/fiber-docs/articles/filtering_reads.html)
  - [Working with Occupancies and Accessibilities](https://mglab.gitlab.io/fiber-docs/articles/occupancy.html)
  - [Individual Reads Plot](https://mglab.gitlab.io/fiber-docs/articles/plot_individual_reads.html)
  - [Working with genomic loci and intervals](https://mglab.gitlab.io/fiber-docs/articles/genomic_loci.html)
  - [Gene Body Reads](https://mglab.gitlab.io/fiber-docs/articles/gene_body_reads.html)
  - [Sequencing coverage](https://mglab.gitlab.io/fiber-docs/articles/coverage.html)
  - [Gap Statistics](https://mglab.gitlab.io/fiber-docs/articles/gap_statistics.html)
  - [Computational Phasing](https://mglab.gitlab.io/fiber-docs/articles/computational_phasing.html)
  - [SQLite fiber storage](https://mglab.gitlab.io/fiber-docs/articles/sqlite_fiber.html)
- [Get Involved](https://mglab.gitlab.io/fiber-docs/articles/contribute.html) - contribute to the project.

## Citation

If you use `fiber` in your research, please cite us using the following publication:

Mark Boltengagen, Daan Verhagen, Michael Roland Wolff, Elisa Oberbeckmann, Matthias Hanke, Ulrich Gerland, Philipp Korber, Felix Mueller-Planitz
**A single fiber view of the nucleosome organization in eukaryotic chromatin**.
_Nucleic Acids Research_, 2024 Jan 11; 52(1):166-185. DOI: [10.1093/nar/gkad1098](https://doi.org/10.1093/nar/gkad1098).  

You can also find examples of the analyses provided by `fiber` in this publication.

