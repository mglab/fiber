---
title: "Changes and Updates"
---

# fiber 1.2.0 (planned release)
* `pooled_name` is added to the configuration.yaml to add group name during importing.
* `configuration.yaml` is renamed to `description.yaml` for importing.
* Genome coverage for entire genome was excluded from HTML report due to calculation time.
* HTML Report now split into separate modules that can be controlled by `modules` parameter.
* `calc_rds()` is removed. Calculations of `rds` files are moved to the corresponding module.
* `spiking` is changed to `spike_in` in all names.
* `rda` parameter in `report_html()` is removed. RDS files will be always reused 
  if exists or calculated and saved.
* `Fiber` containing multiple modifications and selection of modifications is added.
* `rmd_report` parameter is added to `report_html()` to process custom R markdown files.

# fiber 1.1.0
* Documentation is added.
* Select loci added to `select_fiber`.
* `plot_box_occupancies_pooled()` renamed to `boxplot_occ()`
* Import modifications were changed to "5mCpG" and "6mA" instead of "m" and "a" respectively.
* `select_fiber` is renamed to `select` to select from a `Fiber` object.

