% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/plot_individual_reads.R
\name{expand_site_radius}
\alias{expand_site_radius}
\title{Expand vector to the site radius. 
Increase size of the site for visualization.}
\usage{
expand_site_radius(vec, site_radius = 0)
}
\arguments{
\item{site_radius}{Site radius to expand the site}
}
\description{
Expand vector to the site radius. 
Increase size of the site for visualization.
}
\keyword{internal}
