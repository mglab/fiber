% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/fiber_object.R
\name{flat.fiber}
\alias{flat.fiber}
\title{Return mod_data with sample_name and ref_seq columns}
\usage{
\method{flat}{fiber}(fiber)
}
\arguments{
\item{fiber}{A Fiber object.}
}
\description{
Return mod_data with sample_name and ref_seq columns
}
\seealso{
Other Fiber object: 
\code{\link{as.matrix.fiber}()},
\code{\link{chr2roman}()},
\code{\link{expand.fiber}()},
\code{\link{is.duplicated.fiber}()},
\code{\link{is.fiber}()},
\code{\link{merge_fiber.fiber}()},
\code{\link{merge_fiber_list}()},
\code{\link{rm_dup.fiber}()},
\code{\link{samples2pools}()},
\code{\link{set_sample_names}()},
\code{\link{split_samples.fiber}()},
\code{\link{update.fiber}()}
}
\concept{Fiber object}
