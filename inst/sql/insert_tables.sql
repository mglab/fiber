INSERT INTO "mod_spec" 
(
    [modification_name],
    [modification_base],
    [canonical_base]
)
  VALUES 
(
    :modification_name,
    :modification_base,
    :canonical_base
);
INSERT INTO "ref_seqs" 
(
    [ref_seq],
    [ref_seq_length]
)
  VALUES 
(
    :ref_seq,
    :ref_seq_length
);
INSERT INTO "run_info" 
(
    [run_name], 
    [run_date], 
    [sequencing_platform], 
    [experimentator_name], 
    [experiment_description]
) 
  VALUES 
(
    :run_name, 
    :run_date, 
    :sequencing_platform,
    :experimentator_name,
    :experiment_description
);
INSERT INTO "samples" 
(
    [run_id],
    [sample_name],
    [sampling_date],
    [cell_line_id],
    [modification_enzyme],
    [modification_time],
    [enzyme_amount],
    [flow_cell_model],
    [library_kit],
    [barcoding_kit],
    [barcode],
    [mapping_tool],
    [mapping_tool_version],
    [basecalling_tool],
    [basecalling_tool_version],
    [modification_calling_tool],
    [modification_calling_tool_version],
    [basecalling_model_name],
    [modification_calling_model_name],
    [sample_description],
    [reference_genome],
    [spike_in_seq_name]
)
  VALUES 
(
    (SELECT [run_id] from "run_info" WHERE [run_name] = :run_name),
    :sample_name,
    :sampling_date,
    :cell_line_id,
    :modification_enzyme,
    :modification_time,
    :enzyme_amount,
    :flow_cell_model,
    :library_kit,
    :barcode,
    :mapping_tool,
    :mapping_tool_version,
    :modification_calling_tool,
    :modification_calling_tool_version,
    :modification_calling_model_name,
    :sample_description,
    :reference_genome,
    :spike_in_seq_name
);
INSERT INTO "reads" 
(
    [read_name],
    [ref_seq_id],
    [sample_id],
    [start],
    [end],
    [strand]
)
  VALUES (
    :read_name,
    (SELECT [ref_seq_id] from "ref_seqs" WHERE [ref_seq] = :ref_seq),
    (SELECT [sample_id] from "samples" WHERE [sample_name] = :sample_name),
    :start,
    :end,
    :strand
);
INSERT INTO "mod_data" 
(
    [read_id],
    [position],
    [modification_id],
    [mod_prob]
)
  VALUES 
(
    (SELECT [read_id] from "reads" WHERE [read_name] = :read_name),
    :position,
    (SELECT [modification_id] from "mod_spec" WHERE [modification_name] = :modification_name),
    :mod_prob
);
