SELECT 
    reads.read_name, 
    mod_data.position, 
    mod_data.mod_prob, 
    mod_spec.modification_name 
FROM 
    mod_data 
JOIN 
    reads ON reads.read_id = mod_data.read_id 
JOIN 
    mod_spec ON mod_spec.modification_id = mod_data.modification_id;

SELECT 
    reads.read_name, 
    ref_seqs.ref_seq,
    reads.start, 
    reads.end, 
    reads.strand, 
    samples.sample_name
FROM 
    reads 
JOIN 
    ref_seqs ON ref_seqs.ref_seq_id = reads.ref_seq_id
JOIN 
    samples ON samples.sample_id = reads.sample_id;

SELECT 
    ref_seq,
    ref_seq_length
FROM ref_seqs;

SELECT 
    run_info.run_name,
    samples.sample_name,
    samples.barcode,
    samples.sampling_date,
    samples.sample_description,
    samples.cell_line_id,
    samples.modification_enzyme,
    samples.modification_time,
    samples.enzyme_amount,
    samples.flow_cell_model,
    samples.library_kit,
    samples.barcoding_kit,
    samples.mapping_tool,
    samples.mapping_tool_version,
    samples.basecalling_tool,
    samples.basecalling_tool_version,
    samples.modification_calling_tool,
    samples.modification_calling_tool_version,
    samples.basecalling_model_name,
    samples.modification_calling_model_name,
    samples.reference_genome,
    samples.spike_in_seq_name
FROM 
    samples
JOIN 
    run_info ON run_info.run_id = samples.run_id;

SELECT 
   run_name,
   run_date,
   sequencing_platform,
   experimentator_name,
   experiment_description
FROM run_info;
