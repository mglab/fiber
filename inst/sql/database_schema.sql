CREATE TABLE "run_info"
(
   [run_id] INTEGER PRIMARY KEY NOT NULL
      CHECK ([run_id] = 1),
   [run_name] TEXT NOT NULL,
   [run_date] BLOB NOT NULL,
   [sequencing_platform] TEXT NOT NULL,
   [experimentator_name] TEXT NOT NULL,
   [experiment_description] TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "samples"
(
   [sample_id] INTEGER PRIMARY KEY NOT NULL,
   [run_id] INTEGER NOT NULL,
   [sample_name] TEXT UNIQUE NOT NULL,
   [barcode] TEXT UNIQUE NOT NULL,
   [sampling_date] BLOB NOT NULL,
   [cell_line_id] TEXT,
   [modification_enzyme] TEXT,
   [modification_time] TEXT,
   [enzyme_amount] TEXT,
   [library_kit] TEXT NOT NULL,
   [barcoding_kit] TEXT NOT NULL,
   [flow_cell_model] TEXT,
   [mapping_tool] TEXT NOT NULL,
   [mapping_tool_version] TEXT NOT NULL,
   [basecalling_tool] TEXT NOT NULL,
   [basecalling_tool_version] TEXT NOT NULL,
   [modification_calling_tool] TEXT NOT NULL,
   [modification_calling_tool_version] TEXT NOT NULL,
   [sample_description] TEXT NOT NULL,
   [basecalling_model_name] TEXT NOT NULL,
   [modification_calling_model_name] TEXT,
   [reference_genome] TEXT,
   [spike_in_seq_name] TEXT,
   FOREIGN KEY ([run_id])
      REFERENCES "run_info" ([run_id])
         ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE TABLE "ref_seqs"
(
   [ref_seq_id] INTEGER PRIMARY KEY NOT NULL,
   [ref_seq] TEXT UNIQUE NOT NULL,
   [ref_seq_length] INTEGER NOT NULL
);
CREATE TABLE IF NOT EXISTS "reads"
(
   [read_id] INTEGER PRIMARY KEY NOT NULL,
   [read_name] TEXT UNIQUE NOT NULL,
   [ref_seq_id] INTEGER NOT NULL,
   [start] INTEGER NOT NULL,
   [end] INTEGER NOT NULL,
   [strand] INTEGER NOT NULL,
   [sample_id] INTEGER NOT NULL,
   FOREIGN KEY ([sample_id])
      REFERENCES "samples" ([sample_id])
         ON DELETE NO ACTION ON UPDATE NO ACTION,
   FOREIGN KEY ([ref_seq_id])
      REFERENCES "ref_seqs" ([ref_seq_id])
         ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE TABLE IF NOT EXISTS "mod_spec"
(
   [modification_id] INTEGER PRIMARY KEY NOT NULL,
   [modification_name] TEXT NOT NULL,
   [modification_base] TEXT NOT NULL,
   [canonical_base] TEXT NOT NULL
);
CREATE TABLE "mod_data"
(
   [mod_data_id] INTEGER PRIMARY KEY NOT NULL,
   [read_id] INTEGER NOT NULL,
   [position] INTEGER NOT NULL,
   [modification_id] INTEGER NOT NULL,
   [mod_prob] INTEGER NOT NULL,
   FOREIGN KEY ([read_id]) 
      REFERENCES "reads" ([read_id])
         ON DELETE NO ACTION ON UPDATE NO ACTION,
   FOREIGN KEY ([modification_id])
      REFERENCES "mod_spec" ([modification_id])
         ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE INDEX [genomic_coordinates_idx] 
   ON "reads" ([ref_seq_id], [start], [end]);
CREATE INDEX [read_name_idx] 
   ON "reads" ([read_name]);
