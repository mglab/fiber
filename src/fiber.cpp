#include <Rcpp.h>
#include <vector>
#include <cmath>
using namespace Rcpp;

/*IntegerVector calc_counts:
*    
*    Input: 
*    IntegerVector hit_state_list is a vector containing all states for the read.
*    IntegerVector group_pos_temp contains the indices of s_state as their hit_state.
*    int count_state determines the state to be counted (-1 for unmethylated, 1 for methylated and 0 for ambiguous).
*    
*    Output: 
*    IntegerVector count_list contains the amounts of count_state states between consecutive s_states.
*/

//' @export
// [[Rcpp::export]]
IntegerVector calc_counts(IntegerVector hit_state_list, IntegerVector group_pos_temp, int count_state){
  IntegerVector count_list(group_pos_temp.size()-1);
  for (int i=0; i<group_pos_temp.size()-1;i++) 
  {
    int counter=0;
    
    for (int j=group_pos_temp[i];j<group_pos_temp[i+1]-1;++j) {
      if(hit_state_list[j] == count_state) {++counter;}
    }
    
    count_list[i] = counter;
  }
  return count_list;
}

/*IntegerVector create_xs_gap:
 * 
 *    Input: 
 *    IntegerVector positions contains the positions on the read of the starts and ends of s_groups.
 *    IntegerVector group_pos_temp contains the indices with s_state as their hit_state.
 *    
 *    Output: 
 *    IntegerVector create_xs_gap contains the distances from end of previous s_state group 
 *    (or in case of the first read from the start of the read) to the start of the next s_state group. 
 *    
 */

//' @export
// [[Rcpp::export]]
IntegerVector create_xs_gap(IntegerVector positions, IntegerVector group_pos_temp, int s_state){
  IntegerVector xs_gap(group_pos_temp.size()-1);

    for(int i=0; i<group_pos_temp.size()-1;i++){
      xs_gap[i]=positions[group_pos_temp[i+1]-1]-positions[group_pos_temp[i+1]-2]-1;
    }
  
  return xs_gap;
}

/*IntegerVector hmm:
*    
*    Input: 
*    IntegerVector observations - vector containing modifications states {-1,0,1}.
*    
*    Output: 
*    IntegerVector corrected observations based on the Hidden Markov Model.
*/

//' @export
// [[Rcpp::export]]
IntegerVector hmm(IntegerVector observations){
  
    int num_observations = observations.size();
    int num_states = 2;
    
    // Normalize observations
    for (int i = 0; i < num_observations; i++) {
        observations[i] = observations[i] + 1;
    }
        
    const double initial_probs[2] = { 0.5, 0.5 };
    
    const double transition_probs[2][2] = {
        {0.881, 0.119},
        {0.119, 0.881}
    };
    
    const double emission_probs[2][3] = {
        {0.05, 0.1, 0.85}, // state 0 - no_nucs {noise; umbigues; 1 - umbig - noise}
        {0.85, 0.1, 0.05}  // state 1 - nucs    {1 - umbig - noise; umbigues; noise}
    };
    
    std::vector<std::vector<double>> delta(num_observations, std::vector<double>(num_states, 0.0));
    std::vector<std::vector<int>> psi(num_observations, std::vector<int>(num_states, 0));
    
    // Initialization step
    for (int state = 0; state < num_states; ++state) {
        delta[0][state] = initial_probs[state] * emission_probs[state][observations[0]];
    }

    // Recursion step
    for (int observation = 1; observation < num_observations; ++observation) {
        for (int j = 0; j < num_states; ++j) {
            double max_prob = 0.0;
            int max_i = 0;
            for (int i = 0; i < num_states; ++i) {
                double prob = delta[observation - 1][i] * transition_probs[i][j] * emission_probs[j][observations[observation]];
                if (prob > max_prob) {
                    max_prob = prob;
                    max_i = i;
                }
            }
            delta[observation][j] = max_prob;
            psi[observation][j] = max_i;
        }
    }

    // Initialization step
    for (int state = 0; state < num_states; ++state) {
        delta[0][state] = initial_probs[state] * emission_probs[state][observations[0]];
    }
    
        // Termination step
    int best_path_prob_index = 0;
    for (int i = 1; i < num_states; ++i) {
        if (delta[num_observations - 1][i] > delta[num_observations - 1][best_path_prob_index]) {
            best_path_prob_index = i;
        }
    }

    // Backtracking
    IntegerVector path(num_observations);
    path[num_observations - 1] = best_path_prob_index;
    for (int observation = num_observations - 2; observation >= 0; --observation) {
        path[observation] = psi[observation + 1][path[observation + 1]];
    }
    
    // Transform states to observations
    for (int i = 0; i < num_observations; i++) {
        path[i] = path[i]*-2 + 1;
    }
  
  return path;
}

