#' Convert '+'/'-' strand to +1/-1.
#' @keywords internal
#' @export
format_strand <- function(x) { 
  as.integer(ifelse(x == '+', 1, ifelse(x == '-', -1, ifelse(x == '*', 0, NA))))
}

#' Convert '+'/'-' strand to +1/-1.
#' @keywords internal
#' @export
reformat_strand <- function(x) { 
  ifelse(x == 1, '+', ifelse(x == -1, '-', ifelse(x == 0, '*', NA)))
}

#' Convert probability to integer (0, 255)
#' Note: transformation is done like this to receive same result 
#'       when converting integer probabilities to probabilities 
#'       done by modtoolkit. It will be logical to use as.integer(x*255) here.
#' @keywords internal
#' @export
p2int <- function(x) {
  if (any(x < 0 | x > 1)) {
    stop("Probability is not in the correct range.")
  }
  as.integer(x*256-0.5)
}

#' Convert integer to probability
#' @keywords internal
#' @export
int2p <- function(x) {
  if (any(!is.integer(x) | x < 0 | x > 255)) {
    stop("Probability  is not in the correct range.")
  } 
  (x+0.5)/256
}

#' Transform log-likelihood to probability
#' Note: this is illegal operation but we do it for the bam-files 
#'       conventional representation of integer probabilities 
#'       which are represented as integers from 0 to 255.
#' @references Used with \code{p2int()} to receive integer probabilities.
#' @keywords internal
#' @export
loglik2p <- function(x) {
  if (!is.numeric(x)) {
    stop("The value must be numeric")
  }
  1/(1 + exp(-x))
}


#' Convert integers to modifications
#' @param x A vector with integer probabilities from 0 to 255 (bam file convention).
#' @param left_threshold Leftmost threshold which defines if site is modified (+1 value).
#' @param right_threshold Rightmost threshold which defines if site is unmodified (-1 value).
#'        The space of probabilities between left and right thresholds will be set 
#'        as ambiguous (undefined modification state) (0 value).
#'        REMOVE DEFAULTS:
#'        Default states for CpG methylation: left 20, right 236.
#'        Default states for m6A methylation: left 5, right 10 (currently is in test).
#' @keywords internal
#' @export
int2mod <- function(x, left_threshold = NULL, right_threshold = NULL) {

   if (is.null(left_threshold) | is.null(right_threshold)) {
     stop("Theresholds for modifications are not specified.")
   }
   if (left_threshold > right_threshold | left_threshold < 0 | right_threshold > 255) {
     stop("Threshold set wrong.")
   }
   if (any(!is.integer(x) | x < 0 | x > 255)) {
     stop("Probability is not in the correct range.")
   }

   #FIXME is it better to ommit NA values? any(!is.integer(x[!is.na(x)]) | x[!is.na(x)] < 0 | x[!is.na(x)] > 255))

   x <- ifelse(x <= left_threshold, -1, ifelse(x >= right_threshold, 1, 0))

   return(x)
}

#' Change chromosome names from 'chr01' to 'chrI' in a fiber object
#' @family Fiber object
#' @export
chr2roman <- function(fiber) {
  chr <- c('chr01' = 'chrI',    'chr02' = 'chrII',
           'chr03' = 'chrIII',  'chr04' = 'chrIV',
           'chr05' = 'chrV',    'chr06' = 'chrVI',
           'chr07' = 'chrVII',  'chr08' = 'chrVIII',
           'chr09' = 'chrIX',   'chr10' = 'chrX',
           'chr11' = 'chrXI',   'chr12' = 'chrXII',
           'chr13' = 'chrXIII', 'chr14' = 'chrXIV',
           'chr15' = 'chrXV',   'chr16' = 'chrXVI')
  for (i in seq_along(chr)) {
    fiber$reads <- fiber$reads[ref_seq == names(chr[i]), ref_seq := chr[[i]]]
    fiber$ref_seqs <- fiber$ref_seqs[ref_seq == names(chr[i]), ref_seq := chr[[i]]]
  }
  return(fiber)
}
